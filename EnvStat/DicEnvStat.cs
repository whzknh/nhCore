﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace nhCore
{
    /// <summary>
    /// 环境站列表及在线数
    /// </summary>
    public sealed class DicEnvStat
    {
        /// <summary>
        /// 环境站字典，设备地址为key，设备为值
        /// </summary>
        public Dictionary<byte, DbSensor> EnvStats { get; set; }

        public int OnlineCount
        {
            get
            {
                int count = 0;
                foreach (DbSensor e in EnvStats.Values)
                {
                    if (e.IsOnline)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        public void Init(IDB db)
        {
            if (!db.CheckField("Configs", "DataType"))
            {
                db.AddColumnToTable("Configs", "DataType", "varchar(20)");//兼容老数据库
            }
            //Thread.Sleep(1000);
            //从数据库中读出设备表和配置表，初始化本对象
            EnvStats = db.GetSensors();//'读出气象站
            foreach (DbSensor e in EnvStats.Values)//遍历设备读出要素配置
            {
                e.Configs = db.GetConfigs(e.Address);//读出气象站对应当然要素配置
            }

            List<UInt16> list = ReturnRegAddr();
            foreach (UInt16 addr in list)
            {
                if (!db.CheckField("Datas", $"A{addr}"))
                {
                    db.AddColumnToTable("Datas", $"A{addr}", "double");
                }
            }
            db.Close();
            db.Open();
        }

        /// <summary>
        /// 返回所有设备寄存器不重复list<>
        /// </summary>
        /// <returns></returns>
        private List<UInt16> ReturnRegAddr()
        {
            List<UInt16> list = new List<UInt16>();
            foreach (DbSensor e in EnvStats.Values)
            {
                foreach (var c in e.Configs)
                {
                    list.Add((ushort)c.Index);
                }
            }
            List<UInt16> ret = list.Distinct().ToList();
            ret.Sort();
            return ret;
        }

        #region 单例模式
        private static readonly DicEnvStat _dicEnvStat = new DicEnvStat();

        //显式的静态构造函数用来告诉C#编译器在其内容实例化之前不要标记其类型
        static DicEnvStat() { }
        //声明一个私有的构造方法，让外部无法调用这个类的构造方法
        private DicEnvStat() { }

        /// <summary>
        /// 返回对象单例
        /// </summary>
        public static DicEnvStat Singleton
        {
            get { return _dicEnvStat; }
        }
        #endregion
    }
}