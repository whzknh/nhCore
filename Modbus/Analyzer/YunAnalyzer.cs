﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace nhCore.Modbus
{
    internal abstract class YunAnalyzer : Analyzer
    {
        public YunAnalyzer(byte address)//云返回没有设备地址，从命令中读取
        {
            Address = address;
        }

        protected private byte Address { get; set; }
    }
}
