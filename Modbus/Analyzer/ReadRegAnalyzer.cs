﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore.Modbus
{
    /// <summary>
    /// 读寄存器，03，04控制码解析
    /// </summary>
    public class ReadRegAnalyzer : Analyzer
    {
        public ReadRegAnalyzer()
        {
            Decoders = new List<Decoder>();
        }

        public override object Analysis(object o)
        {
            byte[] byts = o as byte[];
            FrameValid mb = new FrameValid(byts, true);
            int index;
            Weathers ws = new Weathers();
            ws.Address = mb.Address;

            foreach (Decoder d in Decoders)
            {
                Weather w = new Weather
                {
                    Config = d.Config,
                };

                index = w.Config.Index;

                w.Value = d.DeCode(mb);
                w.StrValue = w.Value.HasValue ? d.Convert(w.Value.Value) : "-";

                ws.AddValue(index, w);//时间为index为-1，传感器数据也可能为0
            }
            return ws;
        }
    }
}
