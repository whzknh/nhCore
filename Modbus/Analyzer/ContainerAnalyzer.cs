﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore.Modbus
{
    internal class ContainerAnalyzer : Analyzer
    {
        /// <summary>
        /// 支持的控制码
        /// </summary>
        public HashSet<byte> Code { get; set; }
        /// <summary>
        /// 要解析的控制码通道
        /// </summary>
        public int Passage = 1;
        public ContainerAnalyzer()
        {
            Name = "集装数据解析器";
            Code = new HashSet<byte> { 0x051, 0x52, 0x53, 0x54 };//支持的控制码
        }

        public override object Analysis(object o)
        {
            byte[] byts = (byte[])o;
            FrameValid mb = new FrameValid(byts, true);
            DateTime dateTime = new DateTime(1, 1, 1);
            List<Weathers> listWeathers = new List<Weathers>();


            if (mb == null)
            {
                Debug.WriteLine(Name + "：mb为null");
            }
            else if (!mb.IsValid)
            {
                Debug.WriteLine(Name + "：mb效验失败");
            }
            else
            {
                try
                {
                    int i = 0;
                    int y = 2000 + mb.Data[i];
                    int m = mb.Data[++i];
                    int d = mb.Data[++i];
                    int h = mb.Data[++i];
                    int min = mb.Data[++i];
                    int s = mb.Data[++i];

                    dateTime = new DateTime(y, m, d, h, min, s);
                    dateTime = dateTime.AddMilliseconds(5);//时间存到ACCESS时有误差，+5ms可使10ms级别对齐
                }
                catch (Exception)
                {
                    Debug.WriteLine(Name + "：时间解析失败");
                }
            }


            for (int i = 0; i < 100; i++)
            {
                Weather wt = new Weather
                {
                    Config = new DbConfig
                    {
                        Type = CoreGlobal.Const_time,
                        Index = CoreGlobal.Const_TimeIndex,
                        Display = "时间",
                        Unit = string.Empty,
                    },
                };
                wt.Value = dateTime.Ticks;
#if DEBUG
                wt.StrValue = dateTime.ToString("yyyy-MM-dd HH:mm:ss.ff");
#endif
                dateTime = dateTime.AddMilliseconds(10);
                Weathers ws = new Weathers();
                ws.Address = mb.Address;
                ws.Values.Add(wt.Config.Index, wt);

                Weather wd = new Weather
                {
                    Config = new DbConfig()
                    {
                        Index = (short)(20 - 1 + Passage),//寄存器在20、21、22、24
                        Display = $"震动{Passage}",
                        Digits = 0,
                        Unit = "mV",
                    },
                };

                int dataSite = 6 + i * 2;//加6跳过时间
                Array.Reverse(mb.Data, dataSite, 2);
                Int16 rawValue = BitConverter.ToInt16(mb.Data, dataSite);
                if (rawValue != Int16.MinValue)
                {
                    wd.Value = rawValue / Math.Pow(10, wd.Config.Digits);
                }
                ws.Values.Add(wd.Config.Index, wd);
                listWeathers.Add(ws);
            }
            return listWeathers;
        }
    }
}
