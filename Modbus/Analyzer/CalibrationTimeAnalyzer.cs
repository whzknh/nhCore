﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nhCore.Modbus
{
    /// <summary>
    /// 校时回复解析
    /// </summary>
    public class CalibrationTimeAnalyzer : Analyzer
    {
        /// <summary>
        /// 分析返回帧
        /// </summary>
        /// <param name="byts">返回帧</param>
        /// <returns>ture校时完成，false校时失败</returns>
        public override object Analysis(object o)
        {
            byte[] byts = o as byte[];
            return byts[5] == 0x03;
        }
    }
    //命令：00 10 00 00 00 03 06 年 月 日 时 分 秒 校验1 校验2
    //回复帧：09 10 00 00 00 03 81 40
}
