﻿using ADOX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore.Modbus
{
    /// <summary>
    /// 云单台实时数据解析器
    /// </summary>
    internal class YunOneRtDataAnalyzer : YunAnalyzer
    {
        public YunOneRtDataAnalyzer(byte address) : base(address)
        {
        }

        public override object Analysis(object o)
        {
            Http.BootOneRealTimeData bootOneRealTimeData = (Http.BootOneRealTimeData)o;
            Weathers weathers = CoreGlobal.RealData.FindOrAddWeathers(Address);

            foreach (Decoder d in Decoders)
            {
                Weather w = new Weather
                {
                    Config = d.Config,
                };

                if(d is WeatherDecoder)
                {
                    foreach(var httpW in bootOneRealTimeData.Weathers)
                    {
                        if (w.Config.Display == httpW.Display)
                        {
                            w.Value = httpW.Value;
                        }
                    }
                }
                else if(d is TimeDecoder)
                {
                    w.Value = bootOneRealTimeData.DateTime.Ticks;
                }
                w.StrValue = w.Value.HasValue ? d.Convert(w.Value.Value) : "-";
                weathers.AddValue(w.Config.Index, w);//时间为index为-1，传感器数据也可能为0
            }
            return weathers;
        }
    }
}
