﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nhCore.Modbus
{
    public abstract class Analyzer
    {
        /// <summary>
        /// 分析器名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 要素解析器列表，只有气象和云气象使用
        /// </summary>
        public List<Decoder> Decoders { get; set; } = new List<Decoder>();

        public abstract object Analysis(object o);
    }
}
