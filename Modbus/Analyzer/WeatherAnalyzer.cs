﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nhCore.Modbus
{
    /// <summary>
    /// 气象数据解析器类，解析实时、历史数据
    /// </summary>
    public class WeatherAnalyzer : Analyzer
    {
        public WeatherAnalyzer()
        {
            Name = "气象数据解析器";
            Decoders = new List<Decoder>();
        }

        public override object Analysis(object o)
        {
            byte[] byts = o as byte[];
            FrameValid mb = new FrameValid(byts, true);
            int index;
            Weathers ws = mb.Code == 0x41 ? new Weathers() : CoreGlobal.RealData.FindOrAddWeathers(mb.Address);
            ws.Address = mb.Address;

            foreach (Decoder d in Decoders)
            {
                Weather w = new Weather
                {
                    Config = d.Config,
                };

                index = w.Config.Index;

                w.Value = d.DeCode(mb);
                w.StrValue = w.Value.HasValue ? d.Convert(w.Value.Value) : "-";

                ws.AddValue(index, w);//时间为index为-1，传感器数据也可能为0
            }
            return ws;
        }
    }
}
