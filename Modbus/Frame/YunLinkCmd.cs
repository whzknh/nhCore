﻿using nhCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

//云链接命令与modbus差别大，为共用代码写这2个类
namespace nhCore.Modbus
{
    public abstract class YunCmd : CmdFrame
    {
        protected byte Id { get; set; }

        public YunCmd(byte id, byte address, byte code) : base(address, code)
        {
            Id = id;
        }
    }
    /// <summary>
    /// 云实时数据命令
    /// </summary>
    public class YunRtCmd : YunCmd
    {
        public YunRtCmd(byte id, byte address, byte code = 4) : base(id, address, code)
        {
            Automatic = true;
            WorkDelayTime = 10 * 1000;
            SleepDelayTime = 60 * 1000;
            Analyzer = new YunOneRtDataAnalyzer(address);//云返回没有设备地址，从命令中读取
        }

        public override object GetCmd()
        {
            string s = $"http://yunlink.027010.cn/OpenAPI/GetReal?sn={YunLinkIO.Sn}&pwd={YunLinkIO.Pwd}&id={Id}";
            return (Code,s);
        }
    }

    /// <summary>
    /// 云历史数据命令
    /// </summary>
    public class YunHistoryCmd : YunCmd
    {
        public YunHistoryCmd(byte id, byte address, byte code = 0x41) : base(id, address, code)
        {
            Automatic = true;
            WorkDelayTime = 0;
            SleepDelayTime = 10 * 60 * 1000;
            Analyzer = new YunHistoryDataAnalyzer(address);
        }

        public override void NextCmd() 
        {
            OffSet++;
        }

        public override object GetCmd()
        {
            DateTime dateTime = DateTime.Now;
            DateTime start = new DateTime(dateTime.Year, dateTime.Month, 1).AddMonths(-OffSet);
            DateTime end = start.AddMonths(1).AddSeconds(-1);
            string s = $"http://yunlink.027010.cn/OpenAPI/GetHistory?sn={YunLinkIO.Sn}&pwd={YunLinkIO.Pwd}&id={Id}&start={start:yyyy-MM-dd HH:mm:ss}&end={end:yyyy-MM-dd HH:mm:ss}";
            return (Code, s);
        }
    }
}
