﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nhCore.Modbus.Frame
{
    /// <summary>
    /// 字符串命令类
    /// </summary>
    public abstract class StrCmdFrame : CmdFrame
    {
        public StrCmdFrame() : base(0, 0)
        {
        }
    }

    public class ReadSnCmd : StrCmdFrame
    {
        public override object GetCmd()
        {
            byte[] sendData = System.Text.Encoding.ASCII.GetBytes("rdsn");
            return sendData;
        }

        /// <summary>
        /// 是否是响应的数据
        /// </summary>
        /// <param name = "byts" >数据帧</ param >
        /// < returns ></ returns >
        public override bool IsAnswer(object o)
        {
            bool result = false;
            if (o is byte[])
            {
                byte[] data = o as byte[];
                if (data.Length == 17 && data[0] == 's' && data[1] == 'n' && data[16] == 10)
                {
                    result = true;
                }
            }
            return result;
        }

        public class WriteSnCmd : StrCmdFrame
        {
            private string StrCmd { get; set; }
            public WriteSnCmd(string strCmd) : base()
            {
                StrCmd = strCmd;
            }

            public override object GetCmd()
            {
                return StrCmd;
            }

            /// <summary>
            /// 是否是响应的数据
            /// </summary>
            /// <param name = "byts" >数据帧</ param >
            /// < returns ></ returns >
            public override bool IsAnswer(object o)
            {
                bool result = false;
                if (o is byte[])
                {
                    byte[] data = o as byte[];
                    if (data.Length == 17 && data[0] == 's' && data[1] == 'n' && data[16] == 10)
                    {
                        result = true;
                    }
                }
                return result;
            }
        }
    }
}
