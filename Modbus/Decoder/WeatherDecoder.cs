﻿using System;
using System.Diagnostics;
using System.Text;


namespace nhCore.Modbus
{
    /// <summary>
    /// 解码2字节整数
    /// </summary>
    public class WeatherDecoder : Decoder
    {
        public WeatherDecoder()
        {
            Name = "WeatherDecoder";
            Config = new DbConfig();
        }

        /// <summary>
        /// 转换成文本值
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override string Convert(double value)
        {
            double v = value;
            string result = "";
            string[] strWD = { "北", "东北", "东", "东南", "南", "西南", "西", "西北" };
            double[] douWS = { 0, 0.3, 1.6, 3.4, 5.5, 8, 10.8, 13.9, 17.2, 20.8, 24.5, 28.5, 32.7, 37 };

            if (!string.IsNullOrEmpty(Config.Type))
            {
                switch (Config.Type.ToLower())
                {
                    case "fengxiang":
                        v = (int)((v + 22) % 360 / 45);
                        result = $"({strWD[System.Convert.ToInt32(v)]})";
                        break;

                    case "fengsu":
                        int i;
                        for (i = 0; i < douWS.Length; i++)
                        {
                            if (v < douWS[i])
                            {
                                break;
                            }
                        }

                        if (i == 0)
                        {
                            result = "(-)";
                        }
                        else if (i == douWS.Length)
                        {
                            result = "(高于12级)";
                        }
                        else
                        {
                            result = $"({i - 1}级)";
                        }
                        break;
                    default:
                        break;
                }
            }
            result = string.Format("{0:f" + Config.Digits + "}", value) + Config.Unit + result;
            return result;
        }

        /// <summary>
        /// 解码数据
        /// </summary>
        /// <param name="mb">帧中的数据，方法会改变数据序数</param>
        /// <returns></returns>
        public override double? DeCode(FrameValid mb)
        {
            double? value = null;

            if (mb == null)
            {
                Debug.WriteLine($"{Name}.DeCode()mb为空");
            }
            else if (!mb.IsValid)
            {
                Debug.WriteLine($"{Name}.DeCode()mb效验未通过");
            }
            else
            {
                int dataSite = ReturnDataIndex * 2;
                Array.Reverse(mb.Data, dataSite, 2);
                Int16 rawValue = BitConverter.ToInt16(mb.Data, dataSite);
                if (rawValue != Int16.MinValue)
                {
                    value = (double)rawValue / Math.Pow(10, Config.Digits);
                }
            }
            return value;
        }
    }

    /// <summary>
    /// 解码2字节无符号数
    /// </summary>
    public class Uint16Decoder : WeatherDecoder
    {
        /// <summary>
        /// 解码数据
        /// </summary>
        /// <param name="mb">帧中的数据，方法会改变数据序数</param>
        /// <returns></returns>
        public override double? DeCode(FrameValid mb)
        {
            double? value = null;

            if (mb == null)
            {
                Debug.WriteLine($"{Name}.DeCode()mb为空");
            }
            else if (!mb.IsValid)
            {
                Debug.WriteLine($"{Name}.DeCode()mb效验未通过");
            }
            else
            {
                int dataSite = ReturnDataIndex * 2;
                Array.Reverse(mb.Data, dataSite, 2);
                UInt16 rawValue = BitConverter.ToUInt16(mb.Data, dataSite);
                value = (double)rawValue / Math.Pow(10, Config.Digits);
            }
            return value;
        }
    }

    /// <summary>
    /// 解码4字节整数
    /// </summary>
    public class Int32Decoder : WeatherDecoder
    {
        /// <summary>
        /// 解码数据
        /// </summary>
        /// <param name="mb">帧中的数据，方法会改变数据序数</param>
        /// <returns></returns>
        public override double? DeCode(FrameValid mb)
        {
            double? value = null;

            if (mb == null)
            {
                Debug.WriteLine($"{Name}.DeCode()mb为空");
            }
            else if (!mb.IsValid)
            {
                Debug.WriteLine($"{Name}.DeCode()mb效验未通过");
            }
            else
            {
                int dataSite = ReturnDataIndex * 2;
                Array.Reverse(mb.Data, dataSite, 4);
                Int32 rawValue = BitConverter.ToInt32(mb.Data, dataSite);
                if (rawValue != Int32.MinValue)
                {
                    value = (double)rawValue / Math.Pow(10, Config.Digits);
                }
            }
            return value;
        }
    }

    /// <summary>
    /// 解码4字节浮点
    /// </summary>
    public class FloatReverseDecoder : WeatherDecoder
    {
        /// <summary>
        /// 解码数据
        /// </summary>
        /// <param name="ms"></param>
        /// <returns></returns>
        public override double? DeCode(FrameValid mb)
        {
            double? value = null;

            if (mb == null)
            {
                Debug.WriteLine($"{Name}.DeCode()mb为空");
            }
            else if (!mb.IsValid)
            {
                Debug.WriteLine($"{Name}.DeCode()mb效验未通过");
            }
            else
            {
                int dataSite = ReturnDataIndex * 2;
                Array.Reverse(mb.Data, dataSite, 4);
                float rawValue = BitConverter.ToSingle(mb.Data, dataSite);

                if (rawValue != float.MinValue)
                {
                    value = (double)rawValue / Math.Pow(10, Config.Digits);
                }
            }
            return value;
        }
    }

    /// <summary>
    /// 解码4字节返向浮点
    /// </summary>
    public class FloatDecoder : WeatherDecoder
    {
        /// <summary>
        /// 解码数据
        /// </summary>
        /// <param name="ms"></param>
        /// <returns></returns>
        public override double? DeCode(FrameValid mb)
        {
            double? value = null;

            if (mb == null)
            {
                Debug.WriteLine($"{Name}.DeCode()mb为空");
            }
            else if (!mb.IsValid)
            {
                Debug.WriteLine($"{Name}.DeCode()mb效验未通过");
            }
            else
            {
                int dataSite = ReturnDataIndex * 2;
                //Array.Reverse(mb.Data, dataSite, 4);
                float rawValue = BitConverter.ToSingle(mb.Data, dataSite);

                if (rawValue != float.MinValue)
                {
                    value = (double)rawValue / Math.Pow(10, Config.Digits);
                }
            }
            return value;
        }
    }
}
