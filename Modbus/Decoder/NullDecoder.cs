﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore.Modbus
{
    public class NullDecoder: WeatherDecoder
    {
        public NullDecoder(uint value)
        {
            Name = "空值解码器，针对计算要素";
            Config = new DbConfig();
            _value = value;
        }

        private readonly uint _value;

        public override double? DeCode(FrameValid mb)
        {
            return CoreGlobal.计算要素[_value];
        }
    }
}
