﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nhCore.Modbus
{
    /// <summary>
    /// 解析器接口
    /// </summary>
    public abstract class Decoder
    {
        /// <summary>
        /// 解码器名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 返回帧数据位置，寄存器地址-命令帧的起始地址
        /// </summary>
        public UInt16 ReturnDataIndex { get; set; }

        /// <summary>
        /// 寄存器配置
        /// </summary>
        public DbConfig Config { get; set; }

        /// <summary>
        /// 解析值
        /// </summary>
        /// <param name="mb"></param>
        /// <returns></returns>
        public abstract double? DeCode(FrameValid mb);

        /// <summary>
        /// 转换文本值
        /// </summary>
        /// <param name="value">真实值</param>
        /// <returns></returns>
        public abstract string Convert(double value);
    }
}
