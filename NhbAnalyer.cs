﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore
{
    public class NhbAnalyer
    {
        public static List<DbConfig> DeCodeNhb(string path)
        {
            FileStream fs = File.OpenRead(path);
            byte[] buffer = new byte[32];
            List<DbConfig> ls = new List<DbConfig>();
            Dictionary<byte, List<DbConfig>> cfgs;

            while (fs.Read(buffer, 0, buffer.Length) != 0)
            {
                cfgs = Modbus.ConfigAnalyzer.BytesToDbConfig(buffer, 0);
                foreach (var cf in cfgs[0])
                {
                    ls.Add(cf);
                }
            }
            fs.Close();
            return ls;
        }
    }
}
