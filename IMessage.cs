﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore
{
    /// <summary>
    /// 其他对象向主窗体传递信息的接口
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        /// <param name="text"></param>
        void Error(string text);

        /// <summary>
        /// 普遍信息
        /// </summary>
        /// <param name="text"></param>
        void Message(string text);
    }
}
