﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore
{
    /// <summary>
    /// 数据库查询返回的数据保存类，提高效率
    /// </summary>
    public class ReturnData
    {
        public List<DateTime> DateTimes { get; set; } = new List<DateTime>();
        public List<List<double?>> Datas { get; set; } = new List<List<double?>>();
        /// <summary>
        /// 列对应的config顺序
        /// </summary>
        public List<int> ColumnIndexs { get; set; }

        public ReturnData(OleDbDataReader reader, List<int> columnIndexs)
        {
            ColumnIndexs = columnIndexs;
            double? value;
            DateTimes.Clear();
            Datas.Clear();
            int fieldCount = reader.FieldCount;
            while (reader.Read())
            {
                List<double?> dic = new List<double?>();
                DateTime time;
                if (reader.GetFieldType(0) == typeof(DateTime))
                {
                    time = reader.GetDateTime(0);
                }
                else
                {
                    time = DateTime.Parse(reader.GetString(0));//统计时返回的是字符串时间
                }
                DateTimes.Add(time);

                for (int i = 1; i < fieldCount; i++)//0为时间，数据从1开始
                {
                    object tmp = reader.GetValue(i);//要素字段
                    value = (tmp == DBNull.Value) ? null : (double?)tmp;
                    dic.Add(value);
                }
                Datas.Add(dic);
            }
        }
    }
}
