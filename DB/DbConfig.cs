﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nhCore
{

    /// <summary>
    /// 环境基本要素，基本成员对应Configs表字段
    /// </summary>
    public class DbConfig
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public DbConfig()
        {
            Type = string.Empty;
        }

        #region 对应Config数据表
        /// <summary>
        /// 采集仪设备地址
        /// </summary>
        public byte Address { get; set; }

        /// <summary>
        /// 数据显示名称
        /// </summary>
        public string Display { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 寄存器地址
        /// </summary>
        public Int16 Index { get; set; }

        /// <summary>
        /// 小数位
        /// </summary>
        public int Digits { get; set; }

        /// <summary>
        /// 数据类型
        /// </summary>
        public Type DataType { get; set; }

        /// <summary>
        /// 要素类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 统计方式
        /// </summary>
        public string Statistics { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public double? Max { get; set; }

        /// <summary>
        /// 最小值
        /// </summary>
        public double? Min { get; set; }
        #endregion

        /// <summary>
        /// 要素标题+值+单位
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Display}";
        }
    }
}
