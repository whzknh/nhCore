﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore
{
    public interface IDB
    {
        void Open();

        void Close();

        void ClearData(uint address);

        /// <summary>
        ///  从数据库中读取设置数据
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetSetting();

        /// <summary>
        /// 从数据库中读出所有设备，并以列表返回，内含每个采集仪的设备地址、类型、名称
        /// </summary>
        /// <returns></returns>
        Dictionary<byte, DbSensor> GetSensors();

        /// <summary>
        /// 取对应设备地址的采集仪要素配置
        /// </summary>
        /// <param name="address">设备地址</param>
        /// <returns>要素配置列表</returns>
        List<DbConfig> GetConfigs(uint address);

        bool SaveHistory(Weathers weather);

        bool DeleteSensor(uint address);

        bool AddSensor(DbSensor sensor);

        bool AddConfig(uint address, DbConfig cfg);

        void SetWarning(uint adress, uint index, double? max, double? min);

        bool ModifySensorName(byte address, string nName);

        bool ModifySetting(Dictionary<string, string> setting);

        bool InsertSetting(Dictionary<string, string> setting);
        ReturnData QueryDatas(DbSensor sensor, DateTime begin, DateTime end, int hourOffset = 0);
        ReturnData QueryReportDay(DbSensor sensor, DateTime value1, DateTime value2, int hourOffset = 0);
        ReturnData QueryReportYear(DbSensor sensor, DateTime value1, DateTime value2, int hourOffset = 0);
        ReturnData QueryReportMonth(DbSensor sensor, DateTime value1, DateTime value2, int hourOffset = 0);
        bool CheckField(String sTblName, String sFldName);
        bool AddColumnToTable(String tableName, String fieldName, String dataType, string fieldDef = "");
    }
}
