﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore
{
    public interface IAnalysisToDisplay
    {
        void RealDataDisplay(Weathers weathers);
        void AddEnv(Dictionary<byte, List<DbConfig>> values, Cmd cmd);

        void IgnoreFailedSyncModeClear();
    }
}
