﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nhCore
{
    /// <summary>
    /// Server数据传递到窗体
    /// </summary>
    public interface ISvrListener
    {
        /// <summary>
        /// 数据解析结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="values"></param>
        /// <param name="cmd"></param>
        void AnalysisResult(IO sender, object values, Cmd cmd);
    }
}
