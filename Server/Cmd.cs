﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore
{
    /// <summary>
    /// Modbus命令，动态特性
    /// </summary>
    public class Cmd
    {
        public Cmd(Modbus.CmdFrame sendCmd)
        {
            SentCmd = sendCmd;
            Time = DateTime.Now.AddMilliseconds(SentCmd.DelayTime);
        }

        /// <summary>
        /// 对应的发送命令
        /// </summary>
        public Modbus.CmdFrame SentCmd { get; set; }

        /// <summary>
        /// 设定的发送时间
        /// </summary>
        public DateTime Time { get; private set; }

        /// <summary>
        /// 连续失败次数
        /// </summary>
        public int FailedCount { get; set; }

        /// <summary>
        /// 置位后将从命令列表中删除
        /// </summary>
        public bool IsDelete { get; set; }

        /// <summary>
        /// 更新下一条命令的发送时间
        /// </summary>
        public void RefreshTime()
        {
            Time = DateTime.Now.AddMilliseconds(SentCmd.DelayTime);
        }
    }
}