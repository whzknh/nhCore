﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nhCore
{
    /// <summary>
    /// 解析后的一台设备的气象数据，历史数据直接存入数据库，实时数据进入实时数据全局变量
    /// </summary>
    public class Weathers
    {
        /// <summary>
        /// 设备地址
        /// </summary>
        public byte Address { get; set; }

        /// <summary>
        /// 最后更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 数据值
        /// </summary>
        public Dictionary<int, Weather> Values { get; set; } = new Dictionary<int, Weather>();

        /// <summary>
        /// 字典中加入气象值
        /// </summary>
        /// <param name="reg">寄存器地址，传感器时间为-1</param>
        /// <param name="weather"></param>
        public void AddValue(int reg, Weather weather)
        {
            lock (Values)
            {
                if (Values.ContainsKey(reg))//包含寄存器则更新
                {
                    Values[reg] = weather;
                }
                else//不包含则添加
                {
                    Values.Add(reg, weather);
                }
            }

            UpdateTime = DateTime.Now;
        }
    }
}
