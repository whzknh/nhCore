﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nhCore
{
    /// <summary>
    /// 气象要素
    /// </summary>
    public class Weather
    {
        /// <summary>
        /// 原始值，真实值
        /// </summary>
        public double? Value { get; set; }

        /// <summary>
        /// 文本值，显示值+单位
        /// </summary>
        public string StrValue { get; set; }

        /// <summary>
        /// 附加参数，解码后解码器赋予
        /// </summary>
        public DbConfig Config { get; set; }

        /// <summary>
        /// 要素名：值 单位
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Config.Display}：{StrValue}";
        }
    }
}
