﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore
{
    public class CoreGlobal
    {
        private static TimeSpan outTime = new TimeSpan(600L * 1000 * 10000);
        /// <summary>
        /// 判断为掉线的超时时间
        /// </summary>
        public static TimeSpan OutTime { get => outTime; }
        public static RealData RealData { get; set; }

        public static string Const_time { get; set; } = "time";

        /// <summary>
        /// 时间index为-1
        /// </summary>
        public static Int16 Const_TimeIndex { get; set; } = -1;

        /// <summary>
        /// 24小时雨量在字典中的索引号
        /// </summary>
        public static uint Uint24小时雨量 { get; set; } = 1;
        public static Dictionary<uint, double?> 计算要素 { get; set; } = new Dictionary<uint, double?>();
    }
}
