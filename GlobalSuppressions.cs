﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#DeleteSensor(System.UInt32)~System.Boolean")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#ModifySetting(System.Collections.Generic.Dictionary{System.String,System.String})~System.Boolean")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#ClearData(System.UInt32)")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#SetWarning(System.UInt32,System.UInt32,System.Nullable{System.Double},System.Nullable{System.Double})")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#Close")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.Open")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.MakeNewDatabase(System.String,System.String)~System.Boolean")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#AddConfig(System.UInt32,nhCore.DbConfig)~System.Boolean")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#AddSensor(nhCore.DbSensor)~System.Boolean")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#GetConfigs(System.UInt32)~System.Collections.Generic.List{nhCore.DbConfig}")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.Access.nhCore#IDB#SaveHistory(nhCore.Weathers)~System.Boolean")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.SPIO.#ctor(System.String,System.Int32,System.Int32)")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.SPIO.Close~System.Boolean")]
[assembly: SuppressMessage("Interoperability", "CA1416:验证平台兼容性", Justification = "<挂起>", Scope = "member", Target = "~M:nhCore.SPIO.SP.#ctor(System.String,System.Int32,System.IO.Ports.StopBits)")]
