﻿
/***************************************************************************
 *                  COM口                    Tcp Client
 * Connected        COM口打开                Tcp连接成功
 * DisConnected     COM口关闭                Tcp连接断开
  ***************************************************************************/

namespace nhCore
{
    /// <summary>
    /// IO口监听接口，IO口的属性，在NHServer实现
    /// </summary>
    public interface IIOListener
    {
        /// <summary>
        /// 设备有数据回来
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="client"></param>
        /// <param name="byt"></param>
        void Received(IO sender, object client, byte[] byt);
        /// <summary>
        /// 设备断开连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="client"></param>
        void DisConnected(IO sender, object client);

        /// <summary>
        /// 设备连接上，串口打开或TCP连接上
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="client"></param>
        void Connected(IO sender, object client);

        /// <summary>
        /// 打开失败
        /// </summary>
        /// <param name="text"></param>
        void Error(string text);
    }
}
