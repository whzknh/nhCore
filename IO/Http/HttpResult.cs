﻿using System;
using System.Collections.Generic;

//用于JSON解码，属性名均不能改
namespace nhCore.Http
{
    /// <summary>
    /// 返回基础
    /// </summary>
    public class ResultBase
    {
        public int State { get; set; }
        public string Message { get; set; }
    }

    /// <summary>
    /// 单台设备返回基础
    /// </summary>
    public class OneResultBase : ResultBase
    {
        public byte ID { get; set; }
        public string Device { get; set; }
    }

    public class Weathers
    {
        public double? Value { get; set; }
        public int Index { get; set; }
        public string Display { get; set; }
        public string Unit { get; set; }
    }

    public class RealTimeDatas
    {
        public byte ID { get; set; }
        public string Device { get; set; }
        public DateTime DateTime { get; set; }
        public List<Weathers> Weathers { get; set; }
    }

    public class BootAllRealTimeData : ResultBase
    {
        public List<RealTimeDatas> Datas { get; set; }
    }

    public class BootOneRealTimeData : OneResultBase
    {
        public DateTime DateTime { get; set; }
        public List<Weathers> Weathers { get; set; }
    }
}