﻿using System;
using System.Collections.Generic;


namespace nhCore.Http
{


    public class BootHistoryData : OneResultBase
    {
        public List<OpenApiHistoryData> Data { get; set; } = new List<OpenApiHistoryData>();
    }

    public class DevConfig
    {
        public int Index { get; set; }

        public string Display { get; set; }

        public string Unit { get; set; }
    }

    public class OpenApiHistoryData : DevConfig
    {
        public Dictionary<DateTime, double?> Values { get; set; }

    }
}