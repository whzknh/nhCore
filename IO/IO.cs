﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhCore
{
    /// <summary>
    /// IO口接口，通用IO口应实现的功能，监听单独接口
    /// </summary>
    public abstract class IO
    {
        #region "属性"
        /// <summary>
        /// IO对象名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// IO监听器
        /// </summary>
        public IIOListener IOListener { get; set; }

        /// <summary>
        /// IO是否打开
        /// </summary>
        public bool IsOpened { get; set; }

        /// <summary>
        /// 回应超时时间
        /// </summary>
        public int TimeOut { get; set; }
        #endregion

        #region"方法"
        /// <summary>
        /// 打开IO
        /// </summary>
        /// <param name="asyncComm">是否是异步通讯</param>
        /// <returns></returns>
        public abstract bool  Open(bool asyncComm = false);

        /// <summary>
        /// 关闭IO
        /// </summary>
        /// <returns></returns>
        public abstract bool Close();

        /// <summary>
        /// 发送命令后等待接受
        /// </summary>
        /// <param name="client">对应的连接</param>
        /// <param name="writeData">发送帧</param>
        /// <param name="readData">接受帧</param>
        public abstract object SendAccept(object client, object writeData);

        /// <summary>
        /// 发送命令
        /// </summary>
        /// <param name="client">对应的连接</param>
        /// <param name="writeData">发送帧</param>
        public abstract bool Send(object client, in object writeData);

        /// <summary>
        /// 设置监听器
        /// </summary>
        /// <param name="l">IO监听</param>
        public void SetIOListener(IIOListener l)
        {
            IOListener = l;
        }
        #endregion
    }
}
