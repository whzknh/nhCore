﻿using System;
using System.Net.Sockets;
using System.Linq;
using System.Text;

namespace nhCore
{
    /// <summary>
    /// 读异步返回类，TCP回调时使用
    /// </summary>
    public class ReadAsyncResult
    {
        public byte[] Buffer { private set; get; }
        public TcpClient Client { get; set; }

        /// <summary>
        /// 构造函数，新建一个读异步返回实例
        /// </summary>
        /// <param name="aIntBufferLength">接收缓冲区长度</param>
        /// <param name="aTcpClient"></param>
        public ReadAsyncResult(int aIntBufferLength, TcpClient aTcpClient)
        {
            this.Buffer = new byte[aIntBufferLength];
            this.Client = aTcpClient;
        }
    }
}
